<?php
/**
 * Template Name: Home Template
 */
?>

<section id="first-top">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-box">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/text_symbol_color.svg">
        <h1>Beteende-terapeutiska föreningen</h1>
        <p>
          <?php the_field('btf-text'); ?>
          </p>
        <a href="/om-kognitiv-beteendeterapi" class="button">
          Läs mer om kbt
        </a>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-xs-12">
        <div class="down-button">
          <div class="down-text">
            Olika besvär och behandling
          </div>
          <div class="down-chavron"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="first-content">
  <h2>Olika besvär och behandling</h2>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p>
          <?php the_field('besvar-text'); ?>
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="circles-title">
          klicka på rubrikerna nedan för att läsa hur KBT används
        </div>
      </div>
    </div>
    <div class="row">
      <?php
        $query_args = array(
            'post_type' => 'behandling',
            'orderby' => 'title',
            'order' => 'ASC',
            'post_status' => 'publish'
          );
        $the_query = new WP_Query( $query_args );
      ?>
      <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
            <a href="<?php the_permalink(); ?>" class="circle">
              <p><?php the_title(); ?></p>
            </a>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>

    </div>
  </div>
</section>

