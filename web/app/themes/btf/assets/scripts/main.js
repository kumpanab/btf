/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        FastClick.attach(document.body);

        if (typeof phpVar !== 'undefined') {
          if (phpVar.indexOf('<!--more-->') > -1) {
            $('.page-text').html(phpVar.substr(0, phpVar.indexOf('<!--more-->')));
            var limitedHeight = $('.page-text').height();
            $('.page-text').html(phpVar);
            var fullHeight = $('.page-text').height();
            $('.page-text').height(limitedHeight);
            $('.show-more').css('display', 'block');
          }
        }

        $('.show-more').click(function(){
          if ($('.show-more').html() == 'Visa mindre info') {
            $('.show-more').html('Visa mer info');
            $('.page-text').stop().animate({
                height: limitedHeight,
            }, 1000);
          }else{
            $('.show-more').html('Visa mindre info');
            $('.page-text').stop().animate({
                height: fullHeight,
            }, 1000);
          }
        });

        $('#primary-menu').on('show.bs.offcanvas', function (e) {
          $('#black-overlay').fadeIn(100);
        });
        $('#primary-menu').on('hide.bs.offcanvas', function (e) {
          $('#black-overlay').fadeOut(100);
        });

        if ($('#menu-info-page-menu').length) {
          var pageTitle = $('.entry-title-container h1').html();
          $('#menu-info-page-menu li a:contains("' + pageTitle + '")').parent().remove();
        };


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        $('#first-top').height($(window).height() - $('header').height());
        $('#first-top').css('margin-top', $('header').height() + 'px');

        $('.down-button').click(function(){
          $('html, body').animate({
            scrollTop: $('#first-content').offset().top
          }, 1000);
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
