<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <?php
      $imageURL = get_field('topp-bild');
      if ($imageURL == '') {
        $imageURL = get_template_directory_uri().'/dist/images/page-top-back.jpg';
      }
    ?>
    <section id="page-top" style="background: url('<?php echo $imageURL; ?>') no-repeat center center; background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="entry-title-container">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="page-content">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8">
            <div class="intro-text">
              <?php the_field('ingress'); ?>
            </div>
            <?php wp_localize_script('sage/js', 'phpVar', get_field('text')); ?>
            <div class="page-text">
              <?php the_field('text'); ?>
            </div>
            <a class="button show-more">Visa mer info</a>
          </div>
          <div class="col-xs-12 col-lg-push-1 col-sm-4 col-lg-3 col-lg-push-1 page-side">
            <a href="javascript:window.print()" class="print-button">Skriv ut sida<img src="<?= get_template_directory_uri(); ?>/dist/images/icon_print.svg"></a>
            <?php if( have_rows('externa_kallor') ): ?>
              <h3>Externa källor</h3>
              <ul>
                <?php while ( have_rows('externa_kallor') ) : the_row(); ?>
                  <li><a href="<?php the_sub_field('lank-adress'); ?>" target="_blank"><span><?php the_sub_field('lank-text'); ?> ›</span></a></li>
                <?php endwhile; ?>
              </ul>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  </article>
<?php endwhile; ?>
