<div class="navbar-fixed-top">
  <header class="banner">
      <div class="container">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?= get_template_directory_uri(); ?>/dist/images/btf_symbol_color.svg"></a>
        <div class="menu-container" data-toggle="offcanvas" data-target="#primary-menu">
          <img src="<?= get_template_directory_uri(); ?>/dist/images/icon_hamburger.svg">
          <?php
            $pageTitle = '';
            $pageTitleBreaker = '';
            if (get_the_title() != 'Startsida') {
              $pageTitle = get_the_title();
              $pageTitleBreaker = ' | ';
            }
          ?>
          <span>Meny<?php echo $pageTitleBreaker; ?><div class="page-title"><?php echo $pageTitle ?></div></span>
        </div>
      </div>
  </header>
</div>

<div id="primary-menu" class="navmenu navmenu-default navmenu-fixed-left offcanvas">
  <div class="cross-container-container close-button" data-toggle="offcanvas" data-target="#primary-menu"><div class="cross-container"><div class="cross"></div></div></div>
  <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navmenu-nav','walker' => new Walker_Nav_Menu));
    endif;
  ?>
</div>
<div id="black-overlay"></div>


<!--         <nav class="nav-primary">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
          endif;
          ?>
        </nav> -->
