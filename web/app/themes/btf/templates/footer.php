<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <a><?php the_field('row1', 'option'); ?></a>
        <a><?php the_field('row2', 'option'); ?></a>
        <a><?php the_field('row3', 'option'); ?></a>
        <p><?php the_field('row4', 'option'); ?></p>
        <p class="lower-case">
          Copyright © <?php echo date("Y"); ?>
        </p>

      </div>
      <div class="col-xs-12 col-sm-6 image-container">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/btf_symbol_text_black_grey.svg">
      </div>
    </div>
  </div>
</footer>
